from setuptools import setup, find_packages

REQUIRED_PACKAGES = [
    'flask>=2.0.3',
    'requests>=2.18.4'
]

setup(name='rcombaseserver',
      version='2.0.1',
      packages=find_packages(exclude=('tests',)),
      url='https://bitbucket.org/truedmp/rcombaseserver',
      include_package_data=True,
      description='base recommender',
      author='Peeranat Fupongsiripan',
      author_email='Peeranat_Fup@truecorp.co.th',
      install_requires=REQUIRED_PACKAGES,
      dependency_links=[
              'git+https://phasathorn_suw@bitbucket.org/truedmp/rcommsutils.git@Release-4.7.7',
              'git+https://phasathorn_suw@bitbucket.org/truedmp/rcomloggingutils.git@Release-1.1.1'],
      zip_safe=False)
