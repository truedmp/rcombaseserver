import time
import requests
from datetime import datetime

from flask import jsonify
from flask.views import MethodView

from . import config as cfg


class RecommenderStats:

    def __init__(self, model, reset_stats_daily):
        self._reset()
        self.model = model
        self.reset_stats_daily = reset_stats_daily

    def _reset(self):
        self.total_positive_success = 0
        self.total_negative_success = 0
        self.total_failure = 0
        self.acc_positive_success_latency = 0
        self.acc_negative_success_latency = 0
        self.last_reset = datetime.now()

    def check_stats_reset(self):
        if self.reset_stats_daily: # reset stats after one day has passed
            delta = datetime.now() - self.last_reset
            if delta.days >= 1:
                self._reset()
        else: # reset stats after every call to stats API
            self._reset()

    def increment_positive_success(self, latency):
        self.total_positive_success += 1
        self.acc_positive_success_latency += latency

    def increment_negative_success(self, latency):
        self.total_negative_success += 1
        self.acc_negative_success_latency += latency

    def increment_failure(self):
        self.total_failure += 1

    def _set_latency_stats(self, stats, stats_type, total_success, acc_success_latency):
        latency_stats = "avg_%s_success_latency" % stats_type
        if total_success > 0:
            stats[latency_stats] = acc_success_latency / total_success
        else:
            stats[latency_stats] = 0

    def get_stats(self):
        """returns recommender statistics.
        """
        total_req = self.total_positive_success + self.total_negative_success + self.total_failure
        stats = dict(model_name=self.model, total_requests=total_req)

        if total_req == 0:
            stats["positive_success_rate"] = 0
            stats["negative_success_rate"] = 0
            stats["failure_rate"] = 0
        else:
            stats["positive_success_rate"] = self.total_positive_success / total_req
            stats["negative_success_rate"] = self.total_negative_success / total_req
            stats["failure_rate"] = self.total_failure / total_req

        self._set_latency_stats(stats, "positive", self.total_positive_success, self.acc_positive_success_latency)
        self._set_latency_stats(stats, "negative", self.total_negative_success, self.acc_negative_success_latency)

        stats["reporting_time"] = time.ctime()
        stats["stat_duration_sec"] = datetime.now().timestamp() - self.last_reset.timestamp()

        self.check_stats_reset()
        return stats


class FIELDS:
    DATA = 'data'
    CHECKED_TIME = 'checked_time'
    STATUS = 'status'
    STATUS_CODE = 'code'
    STATUS_MESSAGE = 'message'


class StatsAPI(MethodView):

    url = '/recommender/stats'

    def get(self):
        """handles HTTP GET request returning statistics.
        """
        stats = {
            FIELDS.DATA: {
                cfg.RCOM_MODEL_NAME: _recommender_stats.get_stats(),
                FIELDS.CHECKED_TIME: int(time.time())
            },
            FIELDS.STATUS: {
                FIELDS.STATUS_CODE: 'Success',
                FIELDS.STATUS_MESSAGE: '%s statistics status success.' % cfg.RCOM_MODEL_NAME.upper()
            }
        }
        return jsonify(stats), requests.codes.ok


def update_success_stats(items, elapsed):
    """A function to update statistics when the request is successful, either positive or negative success"""
    if items:
        _recommender_stats.increment_positive_success(elapsed)
    else:
        _recommender_stats.increment_negative_success(elapsed)


def update_failure_stats():
    """A function to update statistics when the request is unsuccessful, i.e. an exception is thrown while the request
    is being processed"""
    _recommender_stats.increment_failure()


_recommender_stats = RecommenderStats(cfg.RCOM_MODEL_NAME, cfg.RCOM_STAT_RESET_DAILY)
