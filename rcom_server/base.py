import time
import requests

from flask import jsonify
from flask import request
from flask.views import MethodView
from rcomloggingutils.applog_utils import AppLogger, CallDirections, HttpReqMethods

from . import config as cfg
from .stats import update_success_stats, update_failure_stats


_logger = AppLogger(__name__)


def _build_response(items):
    return [
        {
            cfg.RCOM_API_NAME: k,
            cfg.RCOM_API_VALUE: v
        }
        for k, v in items.items()
    ]


def _execute(func, args):
    _logger.debug("executing %s request" % func.__name__,
                  call_direction=CallDirections.Inbound,
                  call_req_params=request.args,
                  call_req_methods=HttpReqMethods.GET)
    start = time.time()
    resp = func(args)
    elapsed = time.time() - start
    _logger.debug("finished executing %s request in %i seconds" % (func.__name__, elapsed))

    return resp


class BaseConfigAPI(MethodView):

    def get(self):
        """handles HTTP GET request for recommender GetConfigs API that subclasses this class."""
        try:
            configs = _execute(self.get_configs, request.args)
            resp_dict = {
                cfg.RCOM_API_CONFIGS: _build_response(configs)
            }

            resp, status = jsonify(resp_dict), requests.codes.ok
        except ValueError as err:
            _logger.warning("Recommender GetConfigs API returns ValueError %s" % err)
            resp, status = "ValueError: %s" % err, requests.codes.bad_request
        except Exception as err:
            _logger.error("Recommender GetConfigs API throws exception %s" % err)
            resp, status = "Exception: %s" % err, requests.codes.server_error

        return resp, status

    def get_configs(self, params):
        raise NotImplementedError("BaseConfigAPI.get_configs not implemented")


class BaseFeatureAPI(MethodView):

    def get(self):
        """handles HTTP GET request for recommender GetFeatures API that subclasses this class."""
        try:
            features = _execute(self.get_features, request.args)
            resp_dict = {
                cfg.RCOM_API_FEATURES: _build_response(features)
            }

            resp, status = jsonify(resp_dict), requests.codes.ok
        except ValueError as err:
            _logger.warning("Recommender GetFeatures API returns ValueError %s" % err)
            resp, status = "ValueError: %s" % err, requests.codes.bad_request
        except Exception as err:
            _logger.error("Recommender GetFeatures API throws exception %s" % err)
            resp, status = "Exception: %s" % err, requests.codes.server_error

        return resp, status

    def get_features(self, params):
        raise NotImplementedError("BaseFeatureAPI.get_features not implemented")


class BaseRecommenderAPI(MethodView):

    def get(self):
        """handles HTTP GET request for recommender API that subclasses this class. This method also logs statistics
        for the recommender."""
        try:
            start_ts_logger = _logger.debug(
                    "Got recommendation request", 
                    call_direction=CallDirections.Inbound,
                    call_req_params=request.args,
                    call_req_methods=HttpReqMethods.GET)
            start = time.time()
            items, msg, payload, pages = self.get_recommendations(request.args)
            elapsed = time.time() - start
            _logger.debug(
                    "Got recommendation response with %i items and msg %s" % (len(items), msg),
                    begin_time=start_ts_logger)

            update_success_stats(items, elapsed)

            resp_dict = {
                cfg.RCOM_API_ITEMS: items,
                cfg.RCOM_API_MSG: msg
            }
            if payload:
                resp_dict[cfg.RCOM_API_PAYLOAD] = payload
            if pages:
                resp_dict[cfg.RCOM_API_PAGES] = pages

            resp, status = jsonify(resp_dict), requests.codes.ok
        except ValueError as err:
            _logger.warning("Recommender returns ValueError %s" % err)
            update_failure_stats()
            resp, status = "ValueError: %s" % err, requests.codes.bad_request
        except Exception as err:
            _logger.error("Recommender throws exception %s" % err)
            update_failure_stats()
            resp, status = "Exception: %s" % err, requests.codes.server_error

        return resp, status

    def get_recommendations(self, params):
        raise NotImplementedError("BaseRecommenderAPI.get_recommendations not implemented")

class BaseFreeResponseAPI(MethodView):

    def get(self):
        """handles HTTP GET request for recommender GetFeatures API that subclasses this class."""
        try:
            response = _execute(self.get_response, request.args)
            resp_dict = response

            resp, status = jsonify(resp_dict), requests.codes.ok
        except ValueError as err:
            _logger.warning("Recommender GetFreeResponse API returns ValueError %s" % err)
            resp, status = "ValueError: %s" % err, requests.codes.bad_request
        except Exception as err:
            _logger.error("Recommender GetFreeResponse API throws exception %s" % err)
            resp, status = "Exception: %s" % err, requests.codes.server_error

        return resp, status

    def get_response(self, params) -> dict:
        raise NotImplementedError("BaseFreeResponseAPI.get_response not implemented")