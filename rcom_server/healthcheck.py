import os
import time
import requests
from urllib.parse import urlsplit

from flask import jsonify
from flask.views import MethodView
from rcommsutils import cassandra_utils, elasticsearch_utils

from . import config as cfg

_ENV_PREFIX = 'RCOM_HEALTHCHECK_'


class FIELDS:
    DATA = 'data'
    DATA_SERVICE = 'service'
    DATA_COMPONENTS = 'components'
    CHECKED_TIME = 'checked_time'
    IP = 'ip'
    STATUS = 'status'
    STATUS_CODE = 'code'
    STATUS_MESSAGE = 'message'


def _get_healthcheck_env_vars():
    """returns a dictionary of environment variables pointing to external APIs to be monitored by the healthcheck service,
    variable's names must start with RCOM_HEALTHCHECK_

    Return:
        dictionary of environment variables
    """
    return dict((k, v) for k, v in os.environ.items() if k.startswith(_ENV_PREFIX))


def _get_es_status():
    """returns a list of connection status to ES nodes.
    """
    results = []
    data = elasticsearch_utils.get_connection_status()
    nodes = data['nodes']
    for node in nodes:
        results.append({
            FIELDS.IP: node[FIELDS.IP].split(':', 1)[0],
            FIELDS.STATUS: node[FIELDS.STATUS],
            FIELDS.CHECKED_TIME: int(time.time())
        })
    return results


def _get_cassandra_status():
    """returns a list of connection status to Cassandra nodes.
    """
    results = []
    status = cassandra_utils.get_connection_status()
    for session in status['sessions']:
        nodes = session['hosts']
        for node in nodes:
            results.append({
                FIELDS.IP: node[FIELDS.IP],
                FIELDS.STATUS: node[FIELDS.STATUS],
                FIELDS.CHECKED_TIME: int(time.time())
            })
    return results


def _get_service_status(url):
    """returns an external API status
    """
    response = requests.get(url)
    status = [{
        FIELDS.IP: "{0.netloc}".format(urlsplit(url)),
        FIELDS.STATUS: 1 if response.status_code == requests.codes.ok else 0,
        FIELDS.CHECKED_TIME: int(time.time())
    }]

    return status


class HealthCheckAPI(MethodView):

    url = '/recommender/healthcheck'

    def get(self):
        """handles HTTP get request returning health check status. the status includes the following
        - components : A connectivity status to the other components (elasticsearch, cassandra, kafka, zk)
        - service : A connectivity to the other services (APIM, External API service)
        """
        services = dict()
        env_vars = _get_healthcheck_env_vars()
        if env_vars:
            for k, v in env_vars.items():
                key = str(k).replace(_ENV_PREFIX, "").lower()
                services[key] = _get_service_status(v)

        resp = {
            FIELDS.DATA: {
                FIELDS.DATA_SERVICE: services,
                FIELDS.DATA_COMPONENTS: {
                    'elasticsearch': _get_es_status(),
                    'cassandra': _get_cassandra_status(),
                    FIELDS.CHECKED_TIME: int(time.time())
                }
            },
            FIELDS.STATUS: {
                FIELDS.STATUS_CODE: 'Success',
                FIELDS.STATUS_MESSAGE: '%s health check status success.' % cfg.RCOM_MODEL_NAME.upper()
            }
        }

        return jsonify(resp), requests.codes.ok
