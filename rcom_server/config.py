import os
import logging

_logger = logging.getLogger(__name__)

RCOM_MODEL_NAME = os.getenv("RCOM_MODEL_NAME", "default_recommender")
RCOM_API_ITEMS = "items"
RCOM_API_MSG = "message"
RCOM_API_PAYLOAD = "payload"
RCOM_API_PAGES = "pages"
RCOM_STAT_RESET_DAILY = bool(os.getenv('RCOM_STAT_RESET_DAILY', False))

RCOM_API_NAME = "name"
RCOM_API_VALUE = "value"
RCOM_API_FEATURES = "features"
RCOM_API_CONFIGS = "configs"