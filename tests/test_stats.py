import json
import requests
import datetime
import unittest as ut

from flask import Flask

from rcom_server import config as cfg
from rcom_server import stats
from rcom_server.stats import StatsAPI, RecommenderStats, update_success_stats, update_failure_stats


class StatsApiTest(ut.TestCase):

    def setUp(self):
        stats._recommender_stats = RecommenderStats(cfg.RCOM_MODEL_NAME, cfg.RCOM_STAT_RESET_DAILY)

    def test_stats_api(self):
        app = Flask(__name__)
        app.config['TESTING'] = True

        update_success_stats([1, 2, 3], 0.5)
        update_success_stats([], 0.5)
        update_failure_stats()

        with app.app_context():
            api = StatsAPI()

            response, status_code = api.get()
            self.assertEqual(requests.codes.ok, status_code)
            actual = json.loads(response.data.decode("utf-8"))
            self.assertTrue('data' in actual)
            self.assertTrue('checked_time' in actual['data'])
            self.assertTrue(cfg.RCOM_MODEL_NAME in actual['data'])

            model_stats = actual['data'][cfg.RCOM_MODEL_NAME]
            self.assertEqual(0.5, model_stats['avg_negative_success_latency'])
            self.assertEqual(0.5, model_stats['avg_positive_success_latency'])
            self.assertAlmostEqual(0.33, model_stats['failure_rate'], 2)
            self.assertEqual(cfg.RCOM_MODEL_NAME, model_stats['model_name'])
            self.assertAlmostEqual(0.33, model_stats['positive_success_rate'], 2)
            self.assertAlmostEqual(0.33, model_stats['negative_success_rate'], 2)
            self.assertTrue('reporting_time' in model_stats)
            self.assertTrue('stat_duration_sec' in model_stats)
            self.assertEqual(3, model_stats['total_requests'])

            exp_hc_status = dict(code='Success', message="%s statistics status success." % cfg.RCOM_MODEL_NAME.upper())
            self.assertDictEqual(exp_hc_status, actual['status'])

    def test_get_stats_no_request_made(self):
        rcom_stats = stats._recommender_stats.get_stats()
        self.assertEqual(cfg.RCOM_MODEL_NAME, rcom_stats['model_name'])
        self.assertEqual(0, rcom_stats['total_requests'])
        self.assertEqual(0, rcom_stats['positive_success_rate'])
        self.assertEqual(0, rcom_stats['negative_success_rate'])
        self.assertEqual(0, rcom_stats['failure_rate'])
        self.assertEqual(0, rcom_stats['avg_positive_success_latency'])
        self.assertEqual(0, rcom_stats['avg_negative_success_latency'])
        self.assertTrue('reporting_time' in rcom_stats)
        self.assertTrue('stat_duration_sec' in rcom_stats)

    def test_get_stats(self):
        update_success_stats([1, 2, 3], 0.5)
        update_success_stats([1, 2, 3], 0.5)
        update_success_stats([1, 2, 3], 0.5)
        update_success_stats([], 0.5)
        update_failure_stats()

        rcom_stats = stats._recommender_stats.get_stats()

        self.assertEqual(cfg.RCOM_MODEL_NAME, rcom_stats['model_name'])
        self.assertEqual(5, rcom_stats['total_requests'])
        self.assertEqual(0.6, rcom_stats['positive_success_rate'])
        self.assertEqual(0.2, rcom_stats['negative_success_rate'])
        self.assertEqual(0.2, rcom_stats['failure_rate'])
        self.assertEqual(0.5, rcom_stats['avg_positive_success_latency'])
        self.assertEqual(0.5, rcom_stats['avg_negative_success_latency'])
        self.assertTrue('reporting_time' in rcom_stats)
        self.assertTrue('stat_duration_sec' in rcom_stats)

    def test_stats_api_url(self):
        self.assertEqual('/recommender/stats', StatsAPI.url)

    def test_update_success_stats_positive_success(self):
        update_success_stats([1, 2, 3], 0.5)
        self.assertEqual(1, stats._recommender_stats.total_positive_success)
        self.assertEqual(0, stats._recommender_stats.total_negative_success)
        self.assertEqual(0.5, stats._recommender_stats.acc_positive_success_latency)
        self.assertEqual(0, stats._recommender_stats.acc_negative_success_latency)

    def test_update_success_stats_negative_success(self):
        update_success_stats([1, 2, 3], 0.5)
        update_success_stats([1, 2, 3], 0.5)
        update_success_stats([1, 2, 3], 0.5)
        update_success_stats([], 0.5)
        self.assertEqual(3, stats._recommender_stats.total_positive_success)
        self.assertEqual(1, stats._recommender_stats.total_negative_success)
        self.assertEqual(1.5, stats._recommender_stats.acc_positive_success_latency)
        self.assertEqual(0.5, stats._recommender_stats.acc_negative_success_latency)

    def test_update_failure_stats(self):
        update_failure_stats()
        update_failure_stats()
        self.assertEqual(2, stats._recommender_stats.total_failure)

    def test_check_stats_reset_no_daily_reset_schedule(self):
        now = datetime.datetime.now()
        stats._recommender_stats = RecommenderStats(cfg.RCOM_MODEL_NAME, False)
        stats._recommender_stats.last_reset = now
        update_success_stats([1, 2, 3], 0.5)
        update_success_stats([], 0.5)
        stats._recommender_stats.check_stats_reset()
        self._assert_init_state()
        self.assertNotEqual(now, stats._recommender_stats.last_reset)

    def test_check_stats_reset_with_daily_reset_schedule_less_than_one_day_passed(self):
        now = datetime.datetime.now()
        stats._recommender_stats = RecommenderStats(cfg.RCOM_MODEL_NAME, True)
        stats._recommender_stats.last_reset = now
        update_success_stats([1, 2, 3], 0.5)
        update_success_stats([], 0.5)
        stats._recommender_stats.check_stats_reset()
        self.assertEqual(1, stats._recommender_stats.total_positive_success)
        self.assertEqual(1, stats._recommender_stats.total_negative_success)
        self.assertEqual(0.5, stats._recommender_stats.acc_positive_success_latency)
        self.assertEqual(0.5, stats._recommender_stats.acc_negative_success_latency)
        self.assertEqual(now, stats._recommender_stats.last_reset)

    def test_check_stats_reset_with_daily_reset_schedule_more_than_one_day_passed(self):
        stats._recommender_stats = RecommenderStats(cfg.RCOM_MODEL_NAME, True)
        stats._recommender_stats.last_reset = datetime.datetime.now() - datetime.timedelta(days=2)
        update_success_stats([1, 2, 3], 0.5)
        update_success_stats([], 0.5)
        stats._recommender_stats.check_stats_reset()
        self._assert_init_state()

    def _assert_init_state(self):
        self.assertEqual(cfg.RCOM_MODEL_NAME, stats._recommender_stats.model)
        self.assertEqual(0, stats._recommender_stats.total_positive_success)
        self.assertEqual(0, stats._recommender_stats.total_negative_success)
        self.assertEqual(0, stats._recommender_stats.total_failure)
        self.assertEqual(0, stats._recommender_stats.acc_positive_success_latency)
        self.assertEqual(0, stats._recommender_stats.acc_negative_success_latency)


if __name__ == '__main__':
    ut.main()