import json
import requests
import unittest as ut
from unittest.mock import patch, MagicMock

from flask import Flask

from rcom_server import healthcheck as hc
from rcom_server import config as cfg


class HealthCheckApiTest(ut.TestCase):
    @patch('rcom_server.healthcheck.os.environ.items')
    def test_get_healthcheck_env_vars(self, mock):
        mock.return_value = iter([('RCOM_HEALTHCHECK_abc', "test"),
                                  ('RCOM_HEALTHCHECK_def', "xxx"),
                                  ('ok', 'yyy')])
        actual = hc._get_healthcheck_env_vars()
        exp = dict(RCOM_HEALTHCHECK_abc="test", RCOM_HEALTHCHECK_def="xxx")
        self.assertDictEqual(exp, actual)

    @patch('rcom_server.healthcheck.time')
    @patch('rcom_server.healthcheck.elasticsearch_utils')
    def test_get_es_status(self, mock_es_utils, mock_time):
        mock_es_utils.get_connection_status.return_value = {
            "nodes": [{
                "ip": "elasticsearch",
                "status": 1,
                "checked_time": mock_time.time.return_value
            }]
        }
        status = hc._get_es_status()
        exp = ['ip', 'status', 'checked_time']
        for item in status:
            self.assertCountEqual(exp, list(item.keys()))

    @patch('rcom_server.healthcheck.time')
    @patch('rcom_server.healthcheck.cassandra_utils')
    def test_get_cassandra_status(self, mock_cassandra_utils, mock_time):
        mock_cassandra_utils.get_connection_status.return_value = {
            "sessions": [{
                "hosts": [{
                    "ip": "cassandra",
                    "status": 1,
                    "checked_time": mock_time.time.return_value
                }]
            }]
        }
        status = hc._get_cassandra_status()
        exp = ['ip', 'status', 'checked_time']
        for item in status:
            self.assertCountEqual(exp, list(item.keys()))

    def test_healthcheckapi_url(self):
        self.assertEqual('/recommender/healthcheck', hc.HealthCheckAPI.url)

    @patch('rcom_server.healthcheck._get_es_status')
    @patch('rcom_server.healthcheck._get_cassandra_status')
    @patch('rcom_server.healthcheck._get_service_status')
    @patch("rcom_server.healthcheck._get_healthcheck_env_vars")
    def test_healthcheckapi(self, _get_healthcheck_env_vars, _get_service_status, _get_cassandra_status, _get_es_status):
        mock_ext_status = [{'checked_time': 1533628137, 'ip': 'www.mock.com', 'status': 1}]
        _get_cassandra_status.return_value = [{'checked_time': 1533628137, 'ip': 'www.mock.com', 'status': 1}]
        _get_es_status.return_value = [{'checked_time': 1533628137, 'ip': 'www.mock.com', 'status': 1}]
        _get_healthcheck_env_vars.return_value = dict(RCOM_HEALTHCHECK_abc="http://www.mock.com")
        _get_service_status.return_value = mock_ext_status

        app = Flask(__name__)
        app.config['TESTING'] = True

        with app.app_context():
            api = hc.HealthCheckAPI()

            response, status_code = api.get()
            self.assertEqual(requests.codes.ok, status_code)
            actual = json.loads(response.data.decode("utf-8"))
            self.assertTrue('data' in actual)
            self.assertTrue(len(actual['data']['components']['cassandra']) > 0)
            self.assertTrue(len(actual['data']['components']['elasticsearch']) > 0)
            self.assertTrue('checked_time' in actual['data']['components'])
            self.assertDictEqual(dict(abc=mock_ext_status), actual['data']['service'])
            _get_service_status.assert_called_once_with('http://www.mock.com')

            exp_hc_status = dict(code='Success', message="%s health check status success." % cfg.RCOM_MODEL_NAME.upper())
            self.assertDictEqual(exp_hc_status, actual['status'])

    params = [(400, 0), (200, 1)]
    @patch('rcom_server.healthcheck.requests')
    def test_get_service_status(self, requests):
        for req_status, resp_status in self.params:
            with self.subTest():
                self._mock_request(requests, req_status)
                url = "http://fakeservice.com/api"
                status = hc._get_service_status(url)

                requests.get.assert_called_with(url)
                exp = ['ip', 'status', 'checked_time']
                for item in status:
                    self.assertCountEqual(exp, list(item.keys()))
                    self.assertEqual(resp_status, item['status'])
                    self.assertEqual("fakeservice.com", item['ip'])

    def _mock_request(self, requests, status):
        response_mock = MagicMock()
        response_mock.status_code = status
        requests.get.return_value = response_mock
        requests.codes.ok = 200


if __name__ == '__main__':
    ut.main()