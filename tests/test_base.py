import json
import unittest as ut
from unittest.mock import call, MagicMock

from flask import Flask
from werkzeug.datastructures import ImmutableMultiDict

from rcom_server import base
from rcom_server import config as cfg
from rcom_server import BaseConfigAPI, BaseFeatureAPI, BaseRecommenderAPI, BaseFreeResponseAPI
from rcomloggingutils.applog_utils import CallDirections, HttpReqMethods


class InvalidRecommenderAPI(BaseRecommenderAPI):
    pass


class InvalidConfigAPI(BaseConfigAPI):
    pass


class InvalidFeatureAPI(BaseFeatureAPI):
    pass

class InvalidFreeResponseAPI(BaseFreeResponseAPI):
    pass

class ExampleRecommenderAPI(BaseRecommenderAPI):

    def get_recommendations(self, kwargs):
        """implementation below is just a mock up to test BaseRecommenderAPI logic."""
        ssoId = kwargs.get('ssoId')
        withPayload = kwargs.get('returnPayload')
        cursor = kwargs.get('cursor')
        if ssoId is None:
            raise ValueError("No ssoId found")
        elif ssoId == 'xxx':
            raise Exception("mock internal server error")
        elif ssoId == 'empty':
            return [], 'Found fewer items', None, None
        payload = None
        pages = None
        if withPayload:
            payload = {"payload": "123"}
        if cursor:
            pages = {"pages": "123"}
        return [1, 2, 3], 'Success', payload, pages


class ExampleFeatureAPI(BaseFeatureAPI):

    def get_features(self, params):
        ssoId = params.get('ssoId')
        if ssoId is None:
            raise ValueError("No ssoId found")

        if 'throw_value_error' in params:
            raise ValueError("mock value error")
        elif 'throw_exception' in params:
            raise Exception("mock internal error")

        features = {
            'watched': ['a', 'b', 'c'],
            'day_period': ['MORNING'],
            "day_of_week": ['MONDAY'],
            "watch_category": ['cat1', 'cat2'],
        }
        return features


class ExampleConfigAPI(BaseConfigAPI):

    def get_configs(self, params):
        configs = {
            'deployed_version': 1,
            'deployed_table': 1,
            'logging_level': 'INFO',
            'es_ip': 'xxx',
            'cassandra_ip': 'yyy'
        }
        if 'throw_value_error' in params:
            raise ValueError("mock value error")
        elif 'throw_exception' in params:
            raise Exception("mock internal error")
        return configs


class ExampleFreeResponseAPI(BaseFreeResponseAPI):

    def get_response(self, params):
        response = {
            'adaptive_ui': "test",
            'message': "success"
        }
        if 'throw_value_error' in params:
            raise ValueError("mock value error")
        elif 'throw_exception' in params:
            raise Exception("mock internal error")
        return response

class BaseResponseAPITest(ut.TestCase):

    @classmethod
    def setUpClass(cls):
        cls._logger = base._logger
        base._logger = MagicMock()
        app = Flask(__name__)
        app.config['TESTING'] = True

        app.add_url_rule('/mock', view_func=ExampleFreeResponseAPI.as_view('test_response_view'))
        app.add_url_rule('/invalid_response', view_func=InvalidFreeResponseAPI.as_view('invalid_test_response_view'))
        cls.client = app.test_client()

    @classmethod
    def tearDownClass(cls):
        base._logger = cls._logger

    def test_response_api_success(self):
        response = self.client.get('/mock')
        exp = {
            'adaptive_ui': "test",
            'message': "success"
        }
        actual = json.loads(response.data.decode("utf-8"))
        self.assertEqual("200 OK", response.status)
        self.assertEqual(exp, actual)

        call1 = call.debug("executing %s request" % BaseFreeResponseAPI.get_response.__name__,
                                                   call_direction=CallDirections.Inbound,
                                                   call_req_params=ImmutableMultiDict([]),
                                                   call_req_methods=HttpReqMethods.GET)
        call2 = call.debug("finished executing %s request in %i seconds" % (BaseFreeResponseAPI.get_response.__name__, 0))
        base._logger.assert_has_calls([call1, call2])

    def test_response_api_internal_server_error_throw_value_error(self):
        response = self.client.get('/mock?throw_value_error')
        self.assertEqual("400 BAD REQUEST", response.status)
        self.assertEqual("ValueError: mock value error", response.data.decode("utf-8"))

    def test_response_api_internal_server_error_throw_exception(self):
        response = self.client.get('/mock?throw_exception')
        self.assertEqual("500 INTERNAL SERVER ERROR", response.status)
        self.assertEqual("Exception: mock internal error", response.data.decode("utf-8"))

    def test_response_api_invalid(self):
        response = self.client.get('/invalid_response')
        self.assertEqual("500 INTERNAL SERVER ERROR", response.status)
        self.assertEqual("Exception: BaseFreeResponseAPI.get_response not implemented",
                         response.data.decode("utf-8"))


class BaseFeatureAPITest(ut.TestCase):

    @classmethod
    def setUpClass(cls):
        cls._logger = base._logger
        base._logger = MagicMock()
        app = Flask(__name__)
        app.config['TESTING'] = True

        app.add_url_rule('/features', view_func=ExampleFeatureAPI.as_view('test_features_view'))
        app.add_url_rule('/invalid_features', view_func=InvalidFeatureAPI.as_view('invalid_test_features_view'))
        cls.client = app.test_client()

    @classmethod
    def tearDownClass(cls):
        base._logger = cls._logger

    def test_build_response(self):
        items = dict(deployed_version=2, deployed_table='xxx', cassandra_ip='yyy', es_ip='abc')
        actual = base._build_response(items)
        exp = [
            {
                cfg.RCOM_API_NAME: 'deployed_version',
                cfg.RCOM_API_VALUE: 2
            },
            {
                cfg.RCOM_API_NAME: 'deployed_table',
                cfg.RCOM_API_VALUE: 'xxx'
            },
            {
                cfg.RCOM_API_NAME: 'cassandra_ip',
                cfg.RCOM_API_VALUE: 'yyy'
            },
            {
                cfg.RCOM_API_NAME: 'es_ip',
                cfg.RCOM_API_VALUE: 'abc'
            }
        ]
        self.assertListEqual(exp, actual)

    def test_features_api_success(self):
        response = self.client.get('/features?ssoId=abc')
        exp = {
            cfg.RCOM_API_FEATURES: [
                {cfg.RCOM_API_NAME: 'watched', cfg.RCOM_API_VALUE: ['a', 'b', 'c']},
                {cfg.RCOM_API_NAME: 'day_period', cfg.RCOM_API_VALUE: ['MORNING']},
                {cfg.RCOM_API_NAME: 'day_of_week', cfg.RCOM_API_VALUE: ['MONDAY']},
                {cfg.RCOM_API_NAME: 'watch_category', cfg.RCOM_API_VALUE: ['cat1', 'cat2']}
            ]
        }
        actual = json.loads(response.data.decode("utf-8"))
        self.assertEqual("200 OK", response.status)
        self.assertEqual(exp, actual)

        call1 = call.debug("executing %s request" % BaseFeatureAPI.get_features.__name__,
                           call_direction=CallDirections.Inbound,
                           call_req_params=ImmutableMultiDict([('ssoId', 'abc')]),
                           call_req_methods=HttpReqMethods.GET)
        call2 = call.debug("finished executing %s request in %i seconds" % (BaseFeatureAPI.get_features.__name__, 0))
        base._logger.assert_has_calls([call1, call2])

    def test_features_api_internal_server_error(self):
        response = self.client.get('/features?ssoId=abc&throw_value_error')
        self.assertEqual("400 BAD REQUEST", response.status)
        self.assertEqual("ValueError: mock value error", response.data.decode("utf-8"))

    def test_features_api_internal_server_error(self):
        response = self.client.get('/features?ssoId=abc&throw_exception')
        self.assertEqual("500 INTERNAL SERVER ERROR", response.status)
        self.assertEqual("Exception: mock internal error", response.data.decode("utf-8"))

    def test_features_api_invalid(self):
        response = self.client.get('/invalid_features')
        self.assertEqual("500 INTERNAL SERVER ERROR", response.status)
        self.assertEqual("Exception: BaseFeatureAPI.get_features not implemented",
                         response.data.decode("utf-8"))

class BaseConfigAPITest(ut.TestCase):

    @classmethod
    def setUpClass(cls):
        cls._logger = base._logger
        base._logger = MagicMock()
        app = Flask(__name__)
        app.config['TESTING'] = True

        app.add_url_rule('/configs', view_func=ExampleConfigAPI.as_view('test_configs_view'))
        app.add_url_rule('/invalid_configs', view_func=InvalidConfigAPI.as_view('invalid_test_configs_view'))
        cls.client = app.test_client()

    @classmethod
    def tearDownClass(cls):
        base._logger = cls._logger

    def test_configs_api_success(self):
        response = self.client.get('/configs')
        exp = {
            cfg.RCOM_API_CONFIGS: [
                {cfg.RCOM_API_NAME: 'deployed_version', cfg.RCOM_API_VALUE: 1},
                {cfg.RCOM_API_NAME: 'deployed_table', cfg.RCOM_API_VALUE: 1},
                {cfg.RCOM_API_NAME: 'logging_level', cfg.RCOM_API_VALUE: 'INFO'},
                {cfg.RCOM_API_NAME: 'es_ip', cfg.RCOM_API_VALUE: 'xxx'},
                {cfg.RCOM_API_NAME: 'cassandra_ip', cfg.RCOM_API_VALUE: 'yyy'}
            ]
        }
        actual = json.loads(response.data.decode("utf-8"))
        self.assertEqual("200 OK", response.status)
        self.assertEqual(exp, actual)

        call1 = call.debug("executing %s request" % BaseConfigAPI.get_configs.__name__,
                                                   call_direction=CallDirections.Inbound,
                                                   call_req_params=ImmutableMultiDict([]),
                                                   call_req_methods=HttpReqMethods.GET)
        call2 = call.debug("finished executing %s request in %i seconds" % (BaseConfigAPI.get_configs.__name__, 0))
        base._logger.assert_has_calls([call1, call2])

    def test_configs_api_internal_server_error_throw_value_error(self):
        response = self.client.get('/configs?throw_value_error')
        self.assertEqual("400 BAD REQUEST", response.status)
        self.assertEqual("ValueError: mock value error", response.data.decode("utf-8"))

    def test_configs_api_internal_server_error_throw_exception(self):
        response = self.client.get('/configs?throw_exception')
        self.assertEqual("500 INTERNAL SERVER ERROR", response.status)
        self.assertEqual("Exception: mock internal error", response.data.decode("utf-8"))

    def test_configs_api_invalid(self):
        response = self.client.get('/invalid_configs')
        self.assertEqual("500 INTERNAL SERVER ERROR", response.status)
        self.assertEqual("Exception: BaseConfigAPI.get_configs not implemented",
                         response.data.decode("utf-8"))

class BaseRecommenderAPI(ut.TestCase):

    @classmethod
    def setUpClass(cls):
        cls._logger = base._logger
        base._logger = MagicMock()
        app = Flask(__name__)
        app.config['TESTING'] = True

        app.add_url_rule('/xxx', view_func=ExampleRecommenderAPI.as_view('test_view'))
        app.add_url_rule('/invalid', view_func=InvalidRecommenderAPI.as_view('invalid_test_view'))
        cls.client = app.test_client()

    @classmethod
    def tearDownClass(cls):
        base._logger = cls._logger

    def test_recommender_api_success(self):
        response = self.client.get('/xxx?ssoId=test')
        exp = dict(items=[1, 2, 3], message='Success')
        actual = json.loads(response.data.decode("utf-8"))
        self.assertEqual("200 OK", response.status)
        self.assertEqual(exp, actual)

    def test_recommender_api_with_payload(self):
        response = self.client.get('/xxx?ssoId=test&returnPayload=true')
        exp = dict(items=[1, 2, 3], message='Success', payload={"payload": "123"})
        actual = json.loads(response.data.decode("utf-8"))
        self.assertEqual("200 OK", response.status)
        self.assertEqual(exp, actual)
    
    def test_recommender_api_with_pages(self):
        response = self.client.get('/xxx?ssoId=test&cursor=xxx')
        exp = dict(items=[1, 2, 3], message='Success', pages={"pages": "123"})
        actual = json.loads(response.data.decode("utf-8"))
        self.assertEqual("200 OK", response.status)
        self.assertEqual(exp, actual)

    def test_recommender_api_value_error_thrown(self):
        response = self.client.get('/xxx')
        self.assertEqual("400 BAD REQUEST", response.status)
        self.assertEqual("ValueError: No ssoId found", response.data.decode("utf-8"))

    def test_recommender_api_internal_server_error(self):
        response = self.client.get('/xxx?ssoId=xxx')
        self.assertEqual("500 INTERNAL SERVER ERROR", response.status)
        self.assertEqual("Exception: mock internal server error", response.data.decode("utf-8"))
        
    def test_recommender_api_fewer_items(self):
        response = self.client.get('/xxx?ssoId=empty')
        exp = dict(items=[], message='Found fewer items')
        actual = json.loads(response.data.decode("utf-8"))
        self.assertEqual("200 OK", response.status)
        self.assertEqual(exp, actual)

    def test_recommender_api_invalid_recommender(self):
        response = self.client.get('/invalid?ssoId=test')
        self.assertEqual("500 INTERNAL SERVER ERROR", response.status)
        self.assertEqual("Exception: BaseRecommenderAPI.get_recommendations not implemented", response.data.decode("utf-8"))


if __name__ == '__main__':
    ut.main()