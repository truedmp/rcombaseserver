# rcom-base-server

Base classes for Recommendation model servers

# Current version

2.0.1

# Release detail

2.0.1
- Add new BaseResponseAPI

2.0.0
- Upgrade to Flask 2.0.3
- BaseRecommenderAPI now required to retun 4 values: items, message, payload, pages

1.1.2

- Upgrade development dependency (`rcommsutils` and `rcomloggingutils`)

1.1.1

- Add GetFeatures API and GetConfigs API (DMPREC-680)